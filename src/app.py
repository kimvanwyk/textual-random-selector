from textual.app import App, ComposeResult
from textual.containers import Grid, Vertical
from textual.reactive import reactive
from textual.screen import Screen
from textual.widgets import Button, Footer, Header, Input, Label, Static

import random


class ValueLabel(Static):
    selectable = reactive(True)
    selected = reactive(False)
    highlighted = reactive(False)

    def watch_selectable(self, selectable: bool):
        if not selectable:
            self.add_class("already_selected")
        else:
            self.remove_class("already_selected")

    def watch_highlighted(self, highlighted: bool):
        if highlighted:
            self.add_class("highlighted")
        else:
            self.remove_class("highlighted")

    def watch_selected(self, selected: bool):
        if selected:
            self.add_class("selected")
        else:
            self.remove_class("selected")


class InputScreen(Screen):
    def compose(self) -> ComposeResult:
        yield Grid(
            Label("Value:"),
            Input(
                id="value",
            ),
            Button("Add", variant="primary", id="add"),
            Button("Cancel", variant="error", id="cancel"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "add":
            self.app.value = self.query_one("#value", Input).value
        else:
            self.app.value = None
        self.app.pop_screen()


class BaseScreen(Screen):
    BINDINGS = [
        # ("h", "highlight_next", "Highlight Next"),
        # ("d", "deselect", "Deselect"),
        # ("p", "prime", "Prime"),
        ("r", "randomise", "Randomly Select"),
    ]

    def __init__(self):
        self.active_labels = []
        self.active_index = None
        super().__init__()

    def compose(self) -> ComposeResult:
        """Called to add widgets to the app."""
        yield Header()
        yield Footer()
        yield Vertical()

    def on_screen_resume(self):
        if self.app.value:
            self.add_value(self.app.value)

    def add_value(self, value):
        vert = self.query_one(Vertical)
        v = ValueLabel(value)
        self.active_labels.append(v)
        vert.mount(v)

    def action_prime(self):
        self.add_value("a")
        self.add_value("b")
        self.add_value("c")
        self.add_value("d")
        self.add_value("e")

    def action_highlight_next(self):
        self.highlight_next()

    def action_deselect(self):
        self.deselect_active()

    def action_randomise(self):
        if not self.active_labels:
            return
        if self.active_index is not None:
            self.deselect_active()
        v = random.randint(35, 65)
        self.num_ticks = v
        self.total_ticks = v
        self.timer = self.set_interval(0.1, self.tick, pause=False)

    def tick(self):
        self.num_ticks -= 1
        if self.num_ticks == 0:
            self.timer.pause()
            self.active_labels[self.active_index].selected = True
        else:
            if self.num_ticks == int(((2 * self.total_ticks) / 3)):
                self.timer.stop()
                self.timer = self.set_interval(0.3, self.tick, pause=False)
            if self.num_ticks == int((self.total_ticks / 4)):
                self.timer.stop()
                self.timer = self.set_interval(0.6, self.tick, pause=False)
            if self.num_ticks == int((self.total_ticks / 6)):
                self.timer.stop()
                self.timer = self.set_interval(1.2, self.tick, pause=False)
            self.highlight_next()

    def highlight_next(self):
        if not self.active_labels:
            return
        if self.active_index is None:
            self.active_index = -1
        if self.active_index >= 0:
            self.active_labels[self.active_index].highlighted = False
        self.active_index += 1
        if self.active_index >= len(self.active_labels):
            self.active_index = 0
        self.active_labels[self.active_index].highlighted = True

    def deselect_active(self):
        self.active_labels[self.active_index].selected = False
        self.active_labels[self.active_index].highlighted = False
        self.active_labels[self.active_index].selectable = False
        self.active_labels.pop(self.active_index)
        self.active_index = -1


class RandomSelector(App):
    CSS_PATH = "app.tcss"
    SCREENS = {"base": BaseScreen(), "input": InputScreen()}

    BINDINGS = [
        ("+", "add_value", "Add Value"),
        ("q", "quit", "Quit"),
    ]

    def __init__(self):
        self.value = None
        super().__init__()

    def on_mount(self) -> None:
        self.push_screen("base")

    def action_add_value(self) -> None:
        self.app.push_screen(InputScreen())

    def action_quit(self) -> None:
        self.exit()


if __name__ == "__main__":
    app = RandomSelector()
    app.run()
